# Voter en agisssant

[Premier événement "Voter en agissant"](vote-action-strasbourg-23-novembre.md)

## À qui est-ce destiné ?

Cette invitation est faite à toutes les personnes qui veulent que notre société progresse pour prendre en compte les défis et les avancées qui sont à notre portée.

Toute personne qui souhaite exprimer son opinion sur les sujets de société, et qui souhaite que sa contribution individuelle amène à une dynamique collective est invitée.

Toute action ayant pour but de discriminer des personnes sur n'importe quel critère est contraire à cette méthode.

## Raison d'être

Nombreux sont les défis et les progrès qui sont à notre portée. À vous d'identifier ceux qui vous semblent importants.

Nombreuses sont les organisations qui œuvrent à solutionner des problèmes de société, mais les moyens engagés par la société sont trop restreints par rapport à ces défis.

Par ailleurs, de nombreuses personnes ont des convictions et attendent de la société qu'elle progresse.

## Proposition : agir

La proposition ici se résume facilement : agissons !

## Agir pour voter

Notre société est équipée d'outils pour coordonner et activer l'action. L'État en est un. La municipalité en est une.

Puisque des élections municipales approchent (printemps 2020), les regards vont être portés sur les opinions des votant·e·s.

En revanche, si on attend encore le prochain vote sans rien faire, c'est autant de mois où les choses n'auront pas changé.

## Agir pour amorcer le changement d'échelle

Ceci est une invitation à agir. Allons proposer notre aide, ponctuelle pour commencer !

Plus il y aura de personnes à donner des coups de mains aux associations, plus on progressera pour résoudre les problèmes de société abordés par ces associations.

Faire grossir les effectifs de gens qui agissent sur un sujet précis dans un cadre associatif peut mener loin. Mais certains objectifs ne pourront être résolus qu'à l'échelle de la société. D'où la suggestion suivante :
 
## Ancrer son action dans la prise de position politique

Gardons trace de chacune de ces actions, pour donner du poids aux propositions politiques.

À l'échelle de la ville, ces propositions ne seront plus quelque chose de creux. Ces propositions seront portées par les associations historiques et par toutes
les personnes qui agissent concrètement.

Ceci est aussi une invitation à ce qu'une ou des listes s'engagent à prendre en
compte ces prises de position des citoyen·ne·s par des actions.

Sous quelle forme cette prise en compte de ces propositions peut-elle avoir lieu ?

À celles et ceux qui se porteront candidat·e·s de le définir et de nous satisfaire.

Voici cependant quelques pistes :

- démocratie liquide
- budget participatif aligné au niveau de particpation des citoyen·ne·s



## Et si jamais le pouvoir politique reste sourd ?

Alors ça aura déjà fait des dizaines d'actions pendant les mois qui précèdent l'échec… n'est-ce pas déjà une victoire ?






