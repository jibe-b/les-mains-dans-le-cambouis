# Vote-action 23/11/2018, Strasbourg

## Description

Ce type de facilitation est une implémentation de ["Voter en agissant"](https://gitlab.com/jibe-b/les-mains-dans-le-cambouis/blob/master/voter-en-agissant.md)

Un rassemblement est prévu, à la suite des marches pour le climat.

À cette occasion, sur un stand sera affiché un calendrier géant 
avec tous les événements-actions liés au climat et à l'environnement dans les environs et ayant lieu au cours de l'hiver.

Des facilitateur·rice·s guideront les gens dans la découverte des actions des associations et collectifs.

Ces facilitateur·rice·s inviteront les participants à réserver une date pour une action et à la noter dans leurs agendas.

Un point covoiturage-libre.fr/MobiCoop sera organisé, pour que des covoiturages s'organisent.

## Personnes concernées et facilitat·rice·eur·s

Les premières personnes concernées seront les personnes participant au rassemblement.

Toutefois, dès lors que ces personnes se seront inscrites pour des actions, ces mêmes personnes seront invitées à aller par groupe de 2 inviter des passantes et des passants à faire de même. De cette manière, les personnes du premier cercle seront impliquées dans la diffusion auprès de personnes n'étant pas autant sensibilisées aux questions de l'environnement et du climat.

Ainsi, les facilitat·eur·rice·s seront d'abord des personnes organisatrices de cette facilitation, puis leurs rangs seront agrandis par la présence de personnes du rassemblement.

## Place du GCO

Le GCO est un problème qui a une dimension d'urgence différente de la problématique d'augmentation de la contribution aux actions des associations et collectifs. Par conséquent, le stand devra être indépendant.

## Matériel

Dans cette présentation, le mot stand est utilisé pour mentionner un repère physique.

Une option possible est d'avoir une remorque (avec un parasol ;) ) sur laquelle est posée le grand calendrier et un classeur avec des informations sur les différentes associations et leurs actions.

Du fait d'un possible grand nombre de personnes au rassemblement, il est sûrement pertinent d'avoir de la place et plusieus personnes pour faire la facilitation.

Une autre possibilité est d'avoir un stand classique.

Banderole ou affiche avec "Mise en réseau et référencement des initiatives locales" + nuage de tags (Associations, évènements, coopératives, projets citoyens, ...).


## Timing

Il peut être pertinent de fixer des horaires pour faire en sorte que les gens viennent rapidement au stand et puissent rapidement aller inviter du monde dans la rue.


## Préparation nécessaire

- [ ] Référencer les événements-actions liés au climat et à l'environnement prévus pendant l'hiver 2018
- [ ] Référencer les associations et collectifs travaillant sur ces thèmes
- [ ] Présenter les événements sur un calendrier géant
- [ ] Mettre en place un poste d'enregistrement à un événement (le mieux est de le faire depuis son smartphone)
